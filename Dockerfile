FROM golang:alpine AS build

# Use older NodeJS version
# https://pkgs.alpinelinux.org/packages?name=nodejs&branch=v3.16&repo=&arch=&maintainer=
ENV ALPINE_MIRROR "https://dl-cdn.alpinelinux.org/alpine"
ENV ALPINE_NODEJS "3.16"
RUN echo "${ALPINE_MIRROR}/v${ALPINE_NODEJS}/main/" >> /etc/apk/repositories
RUN echo "${ALPINE_MIRROR}/v${ALPINE_NODEJS}/community/" >> /etc/apk/repositories

RUN apk add --update git make build-base ca-certificates && \
    apk add --update nodejs=16.20.2-r0 --repository="${ALPINE_MIRROR}/v${ALPINE_NODEJS}/main/" && \
    apk add --update npm=8.10.0-r0 --repository="${ALPINE_MIRROR}/v${ALPINE_NODEJS}/community/" && \
    rm -rf /var/cache/apk/*
RUN node --version
RUN npm install -g yarn

ARG TAG=v0.107.57

RUN git clone --depth 1 --branch $TAG https://github.com/AdguardTeam/AdGuardHome.git /src/AdGuardHome
WORKDIR /src/AdGuardHome

RUN echo 'You should remove the .dockerignore to allow Makefile to be added to the context'
RUN export CHANNEL=release; export VERSION=$(git describe --tags); make build CHANNEL="release" VERSION="$(git describe --tags)"

FROM alpine:latest
LABEL maintainer="AdGuard Team <devteam@adguard.com>"

# Update CA certs
RUN apk --no-cache --update add ca-certificates && \
    rm -rf /var/cache/apk/* && mkdir -p /opt/adguardhome

COPY --from=build /src/AdGuardHome/AdGuardHome /opt/adguardhome/AdGuardHome

EXPOSE 53/tcp 53/udp 67/tcp 67/udp 68/tcp 68/udp 80/tcp 443/tcp 853/tcp 853/udp 3000/tcp

VOLUME ["/opt/adguardhome/conf", "/opt/adguardhome/work"]

ENTRYPOINT ["/opt/adguardhome/AdGuardHome"]
CMD ["-h", "0.0.0.0", "-c", "/opt/adguardhome/conf/AdGuardHome.yaml", "-w", "/opt/adguardhome/work"]
